﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Models
{
    public class ClientesViewModel
    {
        private ApplicationDbContext contexto;

        public ClientesViewModel()
        {
            contexto = new ApplicationDbContext();
            Clientes = new List<ClientesBusqueda>();
        }

        public List<ClientesBusqueda> Clientes { get; set; }

        public void BuscarPorNombre(string busqueda)
        {
            var consulta = from c in contexto.Clientes
                where c.Nombre.Contains(busqueda)
                select new
                {
                    c.ClienteId,
                    c.RFC,
                    c.TipoPersonaSat,
                    Tipo = c.Tipo.NombreTipo ?? "",
                    nombreCliente = c.Nombre,
                    nombreContacto = c.ContactoCliente.NombreCompleto ?? c.Nombre,
                    Correo = (from e in c.Correos where e.Principal select e.Direccion).First() ?? "",
                    Telefono =
                    (from t in c.Telefonos where t.Principal select t.NumeroTelefonico).First() ?? "",
                    Direccion = (from d in c.Direcciones
                                    where d.Principal
                                    select d.Calle + " " + d.NumExterior + " " + d.Colonia).First() ?? ""
                };
            Clientes.Clear();
            if (consulta != null)
            {
                var lclientes = consulta.ToList();
                foreach (var item in lclientes)
                {
                    Clientes.Add(new ClientesBusqueda
                    {
                        ClienteId = item.ClienteId,
                        Nombre = item.nombreCliente,
                        NombreContacto = item.nombreContacto,
                        RFC = item.RFC,
                        Tipo = item.Tipo,
                        TipoPersonaSat = item.TipoPersonaSat,
                        Email = item.Correo,
                        Telefono = item.Telefono,
                        Direccion = item.Direccion
                    });
                }
            }
        }
    }
}